// 3rd-party imports

import React from 'react'

import styled, { injectGlobal } from 'styled-components'

import 'styled-components-tachyons/variables.css'
import tachyons from 'styled-components-tachyons'

// local imports

import PageBase from './page'
import Canvas from './canvas'

import DateRange from './daterange'
import SkillsTools from './skillstools'
import SummaryLine from './summary_line'

// global styles

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese');

  @import url('https://fonts.googleapis.com/css?family=Source+Code+Pro');

  body {
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 13px;

    font-kerning: normal;
  }

  a {
    color: #0366d6;
    text-decoration: none;
  }
`

// components

const Page = PageBase.extend`
  padding: 0.5in;

  display: flex;
  flex-direction: column;
`

const Header = styled.div`
  ${tachyons};

  font-weight: bold;
  font-size: 20px;
  text-align: center;
`

const Contact = styled.div`
  ${tachyons};

  display: flex;
`

const ContactGutter = styled.div`
  flex-grow: 10;
`

const Link = styled.a`
  font-family: 'Source Code Pro', monospace;

  font-weight: 500;
`

const Phone = styled.span`
  font-family: 'Source Code Pro', monospace;

  font-weight: 500;
`

const SubHeader = styled.div`
  ${tachyons};
  font-weight: bold;
  font-size: 15px;
`

const WorkExperience = styled.div`
  ${tachyons};
`

const LocationTime = styled.div`
  ${tachyons};
  display: flex;
`

const LocationTimeGutter = styled.div`
  flex-grow: 10;
`

const Position = styled.div`
  ${tachyons};
  font-style: italic;
`

const SubSubHeader = styled.div`
  ${tachyons};
  font-weight: bold;
`

const Education = styled.div``

const EducationLine = styled.div`
  ${tachyons};
  display: flex;
`

const EducationGutter = styled.div`
  flex-grow: 10;
`

const OpenSource = styled.div`
  ${tachyons};
`

const OpenSourceLine = styled.div`
  ${tachyons};
`

const Personal = () => {
  return (
    <Canvas>
      <Page>
        {/* Header */}

        <Header mb2>{`Wayan Jimmy`}</Header>

        <Contact mb2>
          <strong>{`E-mail:\u00A0\u00A0`}</strong>
          <Link href="mailto:jimmyeatcrab@gmail.com">{`jimmyeatcrab@gmail.com`}</Link>
          <ContactGutter />
          <strong>{`Github:\u00A0\u00A0`}</strong>
          <Link href="https://github.com/wayanjimmy">
            github.com/wayanjimmy
          </Link>
          <ContactGutter />
          <strong>{`Phone:\u00A0\u00A0`}</strong>
          <Phone>{`+62-8595-3909-677`}</Phone>
          <i>{`\u00A0\u00A0(mobile)`}</i>
        </Contact>

        {/* Work Experience */}

        <SubHeader mb2>{'Work Experience'}</SubHeader>

        <WorkExperience mb2>
          <LocationTime mb1>
            <strong>Okadoc</strong>
            <LocationTimeGutter />
            <DateRange start="December 2019" />
          </LocationTime>

          <Position mb2>Back End Developer</Position>

          <SkillsTools>
            PHP, Go
          </SkillsTools>
        </WorkExperience>

        <WorkExperience mb2>
          <LocationTime mb1>
            <strong>Gogoprint</strong>
            <LocationTimeGutter />
            <DateRange start="October 2018" end="December 2019" />
          </LocationTime>

          <Position mb2>Software Engineer</Position>

          <SkillsTools>
            PHP (Symfony 4, Sonata), Javascript (React), MySQL, MongoDB, Docker
          </SkillsTools>

          <SummaryLine>
            Build features and maintain internal marketplace platform in PHP by
            teaching myself Symfony4, and Sonata Framework to enables the
            operation team to track print job assigned to the printing partners
          </SummaryLine>
        </WorkExperience>

        <WorkExperience mb2>
          <LocationTime mb1>
            <strong>Prinzio</strong>
            <LocationTimeGutter />
            <DateRange start="February 2016" end="October 2018" />
          </LocationTime>

          <Position mb2>Software Engineer</Position>

          <SkillsTools>
            PHP (Laravel), Javascript (Vue, React, Redux), MySQL
          </SkillsTools>

          <SummaryLine>
            Develop customer-facing web platform from scratch and design the
            relational database design by myself to enable the customer to
            create order, checkout and do payment
          </SummaryLine>
          <SummaryLine>
            Build internal web platform that improves sales and the operation
            team work together to execute an order by teaching myself ReactJS
          </SummaryLine>
        </WorkExperience>

        <WorkExperience mb2>
          <LocationTime mb1>
            <strong>Skyshi</strong>
            <LocationTimeGutter />
            <DateRange start="May 2015" end="January 2016" />
          </LocationTime>

          <Position mb2>Backend Developer</Position>

          <SkillsTools>
            PHP (Slim, Phalcon, Codeception), API blueprint, Redis, Solr
          </SkillsTools>

          <SummaryLine>
            Build HTTP Rest API for Mataharimall Mobile Apps POC using Slim
            Framework in 3 months and migrate the SDK from drupal based to the
            Phalcon based
          </SummaryLine>
          <SummaryLine>
            Implemented HTTP Response cache using Redis for some endpoints
            results in smaller response time
          </SummaryLine>
        </WorkExperience>

        <WorkExperience mb2>
          <LocationTime mb1>
            <strong>Maxomorra</strong>
            <LocationTimeGutter />
            <DateRange start="January 2014" end="May 2015" />
          </LocationTime>

          <Position mb2>Web Developer</Position>

          <SkillsTools>PHP, Javascript (jQuery)</SkillsTools>

          <SummaryLine>
            Maintain and build new features for its internal web platform that
            handle product creation and serve 795 merchants
          </SummaryLine>
          <SummaryLine>
            Introduce Git as the source version control to the development team
            and implement it into the codebase and workflow
          </SummaryLine>
        </WorkExperience>

        <SubHeader mb2>{'Technical Skills'}</SubHeader>

        <SubSubHeader mb1>{`Programming:`}</SubSubHeader>

        <SummaryLine>{`Strong: JavaScript, PHP, HTML, CSS, SQL`}</SummaryLine>

        <SummaryLine
        >{`Familiar: Bash scripting, Typescript, Go (golang), NodeJS`}</SummaryLine>

        <SubSubHeader
          mb1
        >{`Tools, utilities, and other miscellaneous:`}</SubSubHeader>

        <SummaryLine
        >{`Git (version control), Linux, MySQL, Nginx, Docker, Vagrant, planttext, tmux`}</SummaryLine>

        <SubSubHeader mb1>{`Devops:`}</SubSubHeader>

        <SummaryLine
        >{`Digital Ocean, Amazon Web Services (AWS) S3 & Lightsail, Heroku, GitlabCI`}</SummaryLine>

        <SubHeader mb2 mt2>
          {'Projects'}
        </SubHeader>

        <OpenSource mb2>
          <OpenSourceLine mb1>
            <strong>GrosirObat</strong>
            <span>{`\u00A0\u00A0\u2014\u00A0\u00A0`}</span>
            <Link href="https://github.com/wayanjimmy/grosirobatjs">{`https://github.com/wayanjimmy/grosirobatjs`}</Link>
          </OpenSourceLine>

          <OpenSourceLine>
            A web base Point of Sale specially for small pharmacy built with
            Develop this in my spare time to learn Fullstack Javascript
            development using express, PostgreSQL and ReactJS
          </OpenSourceLine>
        </OpenSource>

        <OpenSource mb2>
          <OpenSourceLine mb1>
            <strong>Javascript Way Book in Bahasa Indonesia</strong>
            <span>{`\u00A0\u00A0\u2014\u00A0\u00A0`}</span>
            <Link href="https://github.com/wayanjimmy/thejsway_id">
              https://github.com/wayanjimmy/thejsway_id
            </Link>
          </OpenSourceLine>

          <OpenSourceLine>
            Help translate the Javascript Way book into Bahasa Indonesia
          </OpenSourceLine>
        </OpenSource>

        <Education>
          <EducationLine mb1>
            <strong>Udayana University</strong>
            <EducationGutter />
            <span>{`Jimbaran, Bali, Indonesia`}</span>
          </EducationLine>
          <EducationLine>
            <span>{`S.TI., Bachelor of Information Technology`}</span>
            <EducationGutter />
            <span>2009 - 2013</span>
          </EducationLine>
        </Education>
      </Page>
    </Canvas>
  )
}

export default Personal
